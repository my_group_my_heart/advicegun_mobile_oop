import 'Ability.dart';
import 'TypeGun.dart';

abstract class Sniper extends TypeGun with Ability {
  Sniper(super.typrgun);
  
}
