import 'attack.dart';
import 'phaseShoot.dart';

class GameGun extends Attack implements phaseShoot {
  String? name;
  String? color;
  int? phase;

  GameGun(String name, String color, int phase, super.typrgun) {
    this.name = name;
    this.color = color;
    this.phase = phase;
    super.typegun = "AWM";
  }
  void advice() {
    print("สวัสดีครับคุณ" +
        name.toString() +
        " ผมจะมาแนะนำปืน " +
        typegun.toString() +
        " สี" +
        color.toString() +
        "กระบอกนี้นะครับ" +
        " ระยะการยิงอยู่ที่ " +
        phase.toString() +
        "mps " +
        "ความสามารถของปืนกระบอกนี้");
  }

  @override
  void computePhaseShoot() {
    print("ระยะการยิง mps");
  }
}
